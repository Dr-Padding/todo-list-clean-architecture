package com.example.todolist.domain.models

data class ToDoEntity(
    val toDoId: Long,
    val toDoTitle: String,
    val toDoCompleted: Int,
    val toDoListId: Long
)