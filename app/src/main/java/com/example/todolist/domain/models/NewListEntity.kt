package com.example.todolist.domain.models

data class NewListEntity(
    val listId: Long,
    val newListTitle: String
)