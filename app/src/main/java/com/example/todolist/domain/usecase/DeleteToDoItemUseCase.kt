package com.example.todolist.domain.usecase

import com.example.todolist.domain.models.ToDoEntity
import com.example.todolist.domain.repository.Repository

class DeleteToDoItemUseCase(
    private val repository: Repository
) {
    suspend operator fun invoke(toDoItem: ToDoEntity) {
        repository.deleteToDoItem(toDoItem)
    }
}