package com.example.todolist.domain.usecase

import com.example.todolist.domain.models.NewListEntity
import com.example.todolist.domain.repository.Repository

class UpdateNewListItemUseCase(
    private val repository: Repository
) {
    suspend operator fun invoke(oldTitle: String, updatedList: NewListEntity) {
        repository.updateNewListItemTitle(oldTitle, updatedList)
    }
}