package com.example.todolist.domain.repository

import com.example.todolist.domain.models.NewListEntity
import com.example.todolist.domain.models.ToDoEntity

interface Repository {

    suspend fun saveNewListItem(newList: NewListEntity)

    suspend fun saveToDoItem(toDoItem: ToDoEntity)

    suspend fun getAllNewListItems(): List<NewListEntity>

    suspend fun getAllToDoItems(listId: Long): List<ToDoEntity>

    suspend fun deleteNewListItem(newListItem: NewListEntity)
    suspend fun deleteToDoItem(toDoItem: ToDoEntity)

    suspend fun updateNewListItemTitle(oldTitle: String, newListItem: NewListEntity)
    suspend fun updateToDoItemTitle(oldTitle: String, toDoItem: ToDoEntity)
    suspend fun updateToDoCheckBoxUseCase(toDoItem: ToDoEntity)
}