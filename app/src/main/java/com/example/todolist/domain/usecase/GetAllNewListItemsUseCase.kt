package com.example.todolist.domain.usecase

import com.example.todolist.domain.models.NewListEntity
import com.example.todolist.domain.repository.Repository

class GetAllNewListItemsUseCase(
    private val repository: Repository
) {
    suspend operator fun invoke(): List<NewListEntity> {
        return repository.getAllNewListItems()
    }
}