package com.example.todolist.domain.usecase

import com.example.todolist.domain.models.ToDoEntity
import com.example.todolist.domain.repository.Repository

class GetAllToDoItemsUseCase(
    private val repository: Repository
) {
    suspend operator fun invoke(listId: Long): List<ToDoEntity> {
        return repository.getAllToDoItems(listId)
    }
}