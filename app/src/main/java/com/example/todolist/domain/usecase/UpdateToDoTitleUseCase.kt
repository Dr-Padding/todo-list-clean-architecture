package com.example.todolist.domain.usecase

import com.example.todolist.domain.models.NewListEntity
import com.example.todolist.domain.models.ToDoEntity
import com.example.todolist.domain.repository.Repository

class UpdateToDoTitleUseCase(
    private val repository: Repository
) {
    suspend operator fun invoke(oldTitle: String, updatedToDo: ToDoEntity) {
        repository.updateToDoItemTitle(oldTitle, updatedToDo)
    }
}