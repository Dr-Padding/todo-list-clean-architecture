package com.example.todolist.domain.usecase

import com.example.todolist.domain.models.NewListEntity
import com.example.todolist.domain.repository.Repository

class SaveNewListItemUseCase(
    private val repository: Repository
) {
    suspend operator fun invoke(newList: NewListEntity) {
        repository.saveNewListItem(newList)
    }
}