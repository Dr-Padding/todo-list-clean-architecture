package com.example.todolist.data.storage.models

data class NewList(
    val listId: Long,
    val listTitle: String
)