package com.example.todolist.data.mappers

import com.example.todolist.data.storage.models.ToDoItem
import com.example.todolist.domain.models.NewListEntity
import com.example.todolist.domain.models.ToDoEntity

fun NewListEntity.mapToStorage(): com.example.todolist.data.storage.models.NewList {
    return com.example.todolist.data.storage.models.NewList(
        listId = this.listId,
        listTitle = this.newListTitle
    )
}

fun com.example.todolist.data.storage.models.NewList.mapToDomain(): NewListEntity {
    return NewListEntity(listId = this.listId, newListTitle = this.listTitle)
}

fun ToDoEntity.mapToStorage(): ToDoItem {
    return ToDoItem(
        toDoId = this.toDoId,
        toDoTitle = this.toDoTitle,
        toDoCompleted = this.toDoCompleted,
        toDoListId = this.toDoListId
    )
}

fun ToDoItem.mapToDomain(): ToDoEntity {
    return ToDoEntity(
        toDoId = this.toDoId,
        toDoTitle = this.toDoTitle,
        toDoCompleted = this.toDoCompleted,
        toDoListId = this.toDoListId
    )
}