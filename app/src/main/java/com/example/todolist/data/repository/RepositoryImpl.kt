package com.example.todolist.data.repository

import com.example.todolist.data.mappers.mapToDomain
import com.example.todolist.data.mappers.mapToStorage
import com.example.todolist.data.storage.DatabaseStorage
import com.example.todolist.domain.models.NewListEntity
import com.example.todolist.domain.models.ToDoEntity
import com.example.todolist.domain.repository.Repository

class RepositoryImpl(
    private val databaseHelper: DatabaseStorage,
) : Repository {

    override suspend fun saveNewListItem(newListItem: NewListEntity) {
        val list = newListItem.mapToStorage()
        databaseHelper.addNewListItem(list)
    }

    override suspend fun saveToDoItem(toDoItem: ToDoEntity) {
        databaseHelper.addToDoItem(toDoItem.mapToStorage())
    }

    override suspend fun getAllNewListItems(): List<NewListEntity> {
        return databaseHelper.getAllNewListItems().map { it.mapToDomain() }

    }

    override suspend fun getAllToDoItems(listId: Long): List<ToDoEntity> {
        return databaseHelper.getAllToDoItems(listId).map { it.mapToDomain() }
    }

    override suspend fun deleteNewListItem(newListItem: NewListEntity) {
        databaseHelper.deleteNewListItem(newListItem.mapToStorage())
    }

    override suspend fun deleteToDoItem(toDoItem: ToDoEntity) {
        databaseHelper.deleteToDoItem(toDoItem.mapToStorage())
    }

    override suspend fun updateNewListItemTitle(oldTitle: String, newListItem: NewListEntity) {
        databaseHelper.updateNewListItemTitle(oldTitle, newListItem.mapToStorage())
    }

    override suspend fun updateToDoItemTitle(oldTitle: String, toDoItem: ToDoEntity) {
        databaseHelper.updateToDoItemTitle(oldTitle, toDoItem.mapToStorage())
    }

    override suspend fun updateToDoCheckBoxUseCase(toDoItem: ToDoEntity) {
        databaseHelper.updateToDoCheckBoxUseCase(toDoItem.mapToStorage())
    }
}
