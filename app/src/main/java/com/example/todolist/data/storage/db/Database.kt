package com.example.todolist.data.storage.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.todolist.data.storage.DatabaseStorage
import com.example.todolist.data.storage.models.NewList
import com.example.todolist.data.storage.models.ToDoItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class Database(
    context: Context
) : SQLiteOpenHelper(
    context,
    DATA_BASE_TITLE,
    null,
    DB_VERSION
), DatabaseStorage {

    companion object {
        const val DATA_BASE_TITLE = "list.db"
        const val DB_VERSION = 1

        const val LISTS_TABLE = "list_table"
        const val LIST_ID = "list_id"
        const val LIST_TITLE = "new_list_title"

        const val TODO_ITEM_TABLE = "todo_item_table"
        const val TODO_ID = "todo_item_id"
        const val TODO_TITLE = "todo_item_title"
        const val TODO_COMPLETED = "todo_item_completed"
        const val TODO_LIST_ID = "td_list_id"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(
            "CREATE TABLE IF NOT EXISTS $LISTS_TABLE (" +
                    "$LIST_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "$LIST_TITLE TEXT)"
        )

        db?.execSQL(
            "CREATE TABLE IF NOT EXISTS $TODO_ITEM_TABLE (" +
                    "$TODO_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "$TODO_TITLE TEXT," +
                    "$TODO_COMPLETED INTEGER," +
                    "$TODO_LIST_ID INTEGER," +
                    "FOREIGN KEY($TODO_LIST_ID) REFERENCES $LISTS_TABLE($LIST_ID) ON DELETE CASCADE)"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    }

    override suspend fun addNewListItem(newList: NewList) = withContext(Dispatchers.IO) {
        val db = writableDatabase
        db.beginTransaction()
        try {
            val values = ContentValues().apply {
                put(LIST_TITLE, newList.listTitle)
            }
            db.insert(LISTS_TABLE, null, values)
            db.setTransactionSuccessful()
        } finally {
            db.endTransaction()
        }
        db.close()
    }

    override suspend fun addToDoItem(toDoItem: ToDoItem) = withContext(Dispatchers.IO) {
        val db = writableDatabase
        db.beginTransaction()
        try {
            val values = ContentValues().apply {
                put(TODO_TITLE, toDoItem.toDoTitle)
                put(TODO_COMPLETED, toDoItem.toDoCompleted)
                put(TODO_LIST_ID, toDoItem.toDoListId)
            }
            db.insert(TODO_ITEM_TABLE, null, values)
            db.setTransactionSuccessful()
        } finally {
            db.endTransaction()
        }
        db.close()
    }


    override suspend fun updateToDoCheckBoxUseCase(toDoItem: ToDoItem) =
        withContext(Dispatchers.IO) {
            writableDatabase.run {
                val value = ContentValues().apply {
                    put(TODO_COMPLETED, toDoItem.toDoCompleted)
                }
                update(TODO_ITEM_TABLE, value, "$TODO_ID=?", arrayOf(toDoItem.toDoId.toString()))
                close()
            }
        }

    override suspend fun getAllNewListItems(): List<NewList> = withContext(Dispatchers.IO) {
        val db = readableDatabase
        val cursor = db.rawQuery("SELECT $LIST_ID, $LIST_TITLE FROM $LISTS_TABLE", null)
        val data = mutableListOf<NewList>()
        if (cursor.moveToFirst()) {
            do {
                val id = cursor.getLong(cursor.getColumnIndex(LIST_ID).coerceAtLeast(0))
                val title = cursor.getString(cursor.getColumnIndex(LIST_TITLE).coerceAtLeast(0))
                data.add(NewList(listId = id, listTitle = title))
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        data
    }

    override suspend fun getAllToDoItems(listId: Long): List<ToDoItem> =
        withContext(Dispatchers.IO) {
            val db = readableDatabase
            val cursor =
                db.rawQuery("SELECT * FROM $TODO_ITEM_TABLE WHERE $TODO_LIST_ID = '$listId'", null)
            val data = mutableListOf<ToDoItem>()
            if (cursor.moveToFirst()) {
                do {
                    val id = cursor.getLong(cursor.getColumnIndex(TODO_ID).coerceAtLeast(0))
                    val title = cursor.getString(cursor.getColumnIndex(TODO_TITLE).coerceAtLeast(0))
                    val completed =
                        cursor.getInt(cursor.getColumnIndex(TODO_COMPLETED).coerceAtLeast(0))
                    val listId =
                        cursor.getLong(cursor.getColumnIndex(TODO_LIST_ID).coerceAtLeast(0))
                    data.add(
                        ToDoItem(
                            toDoId = id,
                            toDoTitle = title,
                            toDoCompleted = completed,
                            toDoListId = listId
                        )
                    )
                } while (cursor.moveToNext())
            }
            cursor.close()
            db.close()
            data
        }

    override suspend fun deleteNewListItem(newListItem: NewList) = withContext(Dispatchers.IO) {
        writableDatabase.beginTransaction()
        try {
            // Delete the related items from TODO_ITEM_TABLE
            writableDatabase.delete(
                TODO_ITEM_TABLE,
                "$TODO_LIST_ID IN (SELECT $LIST_ID FROM $LISTS_TABLE WHERE $LIST_TITLE = ?)",
                arrayOf(newListItem.listTitle)
            )

            // Delete the list from LISTS_TABLE
            writableDatabase.delete(
                LISTS_TABLE,
                "$LIST_TITLE = ?",
                arrayOf(newListItem.listTitle)
            )

            writableDatabase.setTransactionSuccessful()
        } finally {
            writableDatabase.endTransaction()
        }
        writableDatabase.close()
    }

    override suspend fun deleteToDoItem(toDoItem: ToDoItem) = withContext(Dispatchers.IO) {
        val db = writableDatabase
        db.beginTransaction()
        try {
            db.delete(
                TODO_ITEM_TABLE,
                "$TODO_ID = ?",
                arrayOf(toDoItem.toDoId.toString())
            )
            db.setTransactionSuccessful()
        } finally {
            db.endTransaction()
        }
        db.close()
    }


    override suspend fun updateNewListItemTitle(oldTitle: String, updatedListItem: NewList) =
        withContext(Dispatchers.IO) {
            val db = writableDatabase
            db.beginTransaction()
            try {
                val values = ContentValues().apply {
                    put(LIST_TITLE, updatedListItem.listTitle)
                }
                db.update(LISTS_TABLE, values, "$LIST_TITLE = ?", arrayOf(oldTitle))
                db.setTransactionSuccessful()
            } finally {
                db.endTransaction()
            }
            db.close()
        }

    override suspend fun updateToDoItemTitle(oldTitle: String, updatedToDoItem: ToDoItem) =
        withContext(Dispatchers.IO) {
            val db = writableDatabase
            db.beginTransaction()
            try {
                val values = ContentValues().apply {
                    put(TODO_TITLE, updatedToDoItem.toDoTitle)
                }
                db.update(TODO_ITEM_TABLE, values, "$TODO_TITLE = ?", arrayOf(oldTitle))
                db.setTransactionSuccessful()
            } finally {
                db.endTransaction()
            }
            db.close()
        }
}
