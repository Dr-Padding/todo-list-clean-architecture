package com.example.todolist.data.storage

import com.example.todolist.data.storage.models.NewList
import com.example.todolist.data.storage.models.ToDoItem

interface DatabaseStorage {

    suspend fun addNewListItem(newList: NewList)

    suspend fun addToDoItem(toDoItem: ToDoItem)

    suspend fun getAllNewListItems(): List<NewList>

    suspend fun getAllToDoItems(listId: Long): List<ToDoItem>

    suspend fun deleteNewListItem(newListItem: NewList)

    suspend fun deleteToDoItem(toDoItem: ToDoItem)

    suspend fun updateNewListItemTitle(oldTitle: String, updatedListItem: NewList)

    suspend fun updateToDoItemTitle(oldTitle: String, updatedToDoItem: ToDoItem)

    suspend fun updateToDoCheckBoxUseCase(toDoItem: ToDoItem)
}