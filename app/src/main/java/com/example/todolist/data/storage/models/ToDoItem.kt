package com.example.todolist.data.storage.models

data class ToDoItem(
    val toDoId: Long,
    val toDoTitle: String,
    val toDoCompleted: Int,
    val toDoListId: Long
)