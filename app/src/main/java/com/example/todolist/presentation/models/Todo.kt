package com.example.todolist.presentation.models

data class Todo(
    val title: String,
    val isChecked: Boolean
)