package com.example.todolist.presentation.di

import com.example.todolist.domain.usecase.DeleteNewListItemUseCase
import com.example.todolist.domain.usecase.DeleteToDoItemUseCase
import com.example.todolist.domain.usecase.GetAllNewListItemsUseCase
import com.example.todolist.domain.usecase.GetAllToDoItemsUseCase
import com.example.todolist.domain.usecase.SaveNewListItemUseCase
import com.example.todolist.domain.usecase.SaveToDoItemUseCase
import com.example.todolist.domain.usecase.UpdateNewListItemUseCase
import com.example.todolist.domain.usecase.UpdateToDoCheckBoxUseCase
import com.example.todolist.domain.usecase.UpdateToDoTitleUseCase
import org.koin.dsl.module


val domainModule = module {

    factory {
        DeleteToDoItemUseCase(repository = get())
    }

    factory {
        SaveNewListItemUseCase(repository = get())
    }

    factory {
        GetAllNewListItemsUseCase(repository = get())
    }

    factory {
        DeleteNewListItemUseCase(repository = get())
    }

    factory {
        UpdateNewListItemUseCase(repository = get())
    }

    factory {
        SaveToDoItemUseCase(repository = get())
    }

    factory {
        GetAllToDoItemsUseCase(repository = get())
    }

    factory {
        UpdateToDoCheckBoxUseCase(repository = get())
    }

    factory {
        UpdateToDoTitleUseCase(repository = get())
    }
}