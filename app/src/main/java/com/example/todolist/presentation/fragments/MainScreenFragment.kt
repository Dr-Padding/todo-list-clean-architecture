package com.example.todolist.presentation.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.PopupMenu
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.todolist.R
import com.example.todolist.databinding.FragmentMainScreenBinding
import com.example.todolist.domain.models.NewListEntity
import com.example.todolist.presentation.MainViewModel
import com.example.todolist.presentation.ScreenState
import com.example.todolist.presentation.adapters.NewListAdapter
import com.example.todolist.presentation.utils.Constants.NEW_LIST_ID
import com.example.todolist.presentation.utils.Constants.NEW_LIST_TITLE
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainScreenFragment : Fragment(R.layout.fragment_main_screen) {

    private val binding by viewBinding(FragmentMainScreenBinding::bind)
    private val viewModel by viewModel<MainViewModel>()
    private lateinit var adapter: NewListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpAdapter()
        initViews()
        setObservers()
    }

    override fun onResume() {
        initViews()
        super.onResume()
    }

    private fun setUpAdapter() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapter = NewListAdapter(requireContext(),
            titleClickListener = { newListEntity ->
                // Handle title click
                val bundle = bundleOf(
                    NEW_LIST_ID to newListEntity.listId,
                    NEW_LIST_TITLE to newListEntity.newListTitle
                )
                findNavController().navigate(R.id.action_mainScreenFragment_to_toDoListFragment, bundle)

            },
            iconClickListener = { newListEntity, position ->
                // Handle icon click
                val popupMenu = PopupMenu(
                    requireContext(),
                    binding.recyclerView.findViewHolderForAdapterPosition(position)?.itemView?.findViewById(
                        R.id.ivMoreIcon
                    )
                )
                popupMenu.inflate(R.menu.menu_edit_delete)
                popupMenu.setOnMenuItemClickListener { menuItem ->
                    when (menuItem.itemId) {
                        R.id.menu_edit -> {
                            // Handle Edit option

                            val oldTitle = newListEntity.newListTitle

                            val builder = AlertDialog.Builder(requireContext())
                            builder.setTitle(R.string.update_todo_list)
                            val input = EditText(requireContext())
                            input.setSingleLine()
                            input.setText(oldTitle)
                            builder.setView(input)

                            builder.setPositiveButton(R.string.update) { _, _ ->
                                val updatedTitle = input.text.toString()
                                viewModel.updateNewListItemTitle(newListEntity.listId, oldTitle, updatedTitle)
                                viewModel.getAllNewListItems()
                            }
                            builder.setNegativeButton(R.string.cancel) { dialog, _ ->
                                dialog.cancel()
                            }
                            builder.show()
                            true
                        }
                        R.id.menu_delete -> {
                            // Handle Delete option
                            viewModel.deleteNewListItem(newListEntity.listId, newListEntity.newListTitle)
                            viewModel.getAllNewListItems()
                            true
                        }
                        else -> false
                    }
                }
                popupMenu.show()
            }
        )
        binding.recyclerView.adapter = adapter
    }

    private fun initViews() {

        viewModel.getAllNewListItems()

        binding.apply {
            fab.setOnClickListener {
                val builder = AlertDialog.Builder(requireContext())
                builder.setTitle(R.string.add_todo_list)
                val input = EditText(requireContext())
                input.setSingleLine()
                builder.setView(input)

                builder.setPositiveButton(R.string.add) { _, _ ->
                    val enteredString = input.text.toString()
                    viewModel.saveNewListItem(enteredString)
                    viewModel.getAllNewListItems()
                }
                builder.setNegativeButton(R.string.cancel) { dialog, _ ->
                    dialog.cancel()
                }
                builder.show()
            }

            btnSettings.setOnClickListener {
                findNavController().navigate(R.id.action_mainScreenFragment_to_settingsFragment)
            }
        }
    }

    private fun setObservers() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                viewModel.state.onEach { state ->
                    when (state) {
                        is ScreenState.Loading -> showLoading()
                        is ScreenState.NewListSuccess -> updateRVContent(state.items)
                        is ScreenState.Error -> showError(state.message)
                        else -> {}
                    }
                }.launchIn(this)
            }
        }
    }

    private fun updateRVContent(newList: List<NewListEntity>) {
        adapter.setNewContent(newList)
    }

    private fun showLoading() {
        // Show loading state in the UI, e.g., show a progress bar
    }

    private fun showError(message: String) {
        // Show an error message in the UI, e.g., show a toast or display an error view
    }

}