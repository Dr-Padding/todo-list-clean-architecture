package com.example.todolist.presentation.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.todolist.R
import com.example.todolist.databinding.FragmentSettingsBinding
import com.example.todolist.presentation.utils.Constants.COMPLETED_COLOR_BUTTON_NAME
import com.example.todolist.presentation.utils.Constants.COMPLETED_COLOR_SHARED_PREFS
import com.example.todolist.presentation.utils.Constants.NOT_COMPLETED_COLOR_BUTTON_NAME
import com.example.todolist.presentation.utils.Constants.NOT_COMPLETED_COLOR_SHARED_PREFS


class SettingsFragment : Fragment(R.layout.fragment_settings) {

    private val binding by viewBinding(FragmentSettingsBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        val completedColorPreferences = requireContext().getSharedPreferences(
            COMPLETED_COLOR_SHARED_PREFS,
            Context.MODE_PRIVATE
        )
        val completedColorEditor = completedColorPreferences.edit()

        val notCompletedColorPreferences = requireContext().getSharedPreferences(
            NOT_COMPLETED_COLOR_SHARED_PREFS,
            Context.MODE_PRIVATE
        )
        val notCompletedColorEditor = notCompletedColorPreferences.edit()

        binding.apply {

            val rbGreen: RadioButton = root.findViewById(R.id.rbGreen)
            val rbBlue: RadioButton = root.findViewById(R.id.rbBlue)
            val rbYellow: RadioButton = root.findViewById(R.id.rbYellow)

            val rbBrown: RadioButton = root.findViewById(R.id.rbBrown)
            val rbPink: RadioButton = root.findViewById(R.id.rbPink)
            val rbViolet: RadioButton = root.findViewById(R.id.rbViolet)

            when (completedColorPreferences.getString(COMPLETED_COLOR_BUTTON_NAME, "")) {
                "Green" -> rbGreen.isChecked = true
                "Blue" -> rbBlue.isChecked = true
                "Yellow" -> rbYellow.isChecked = true
            }

            when (notCompletedColorPreferences.getString(NOT_COMPLETED_COLOR_BUTTON_NAME, "")) {
                "Brown" -> rbBrown.isChecked = true
                "Pink" -> rbPink.isChecked = true
                "Violet" -> rbViolet.isChecked = true
            }

            radioGroupForCompletedTasks.setOnCheckedChangeListener { _, checkedId ->
                val selectedRadioButton = root.findViewById<RadioButton>(checkedId)
                val selectedButtonName = selectedRadioButton.text.toString()

                completedColorEditor.putString(COMPLETED_COLOR_BUTTON_NAME, selectedButtonName)
                completedColorEditor.apply()
            }

            radioGroupForNotCompletedTasks.setOnCheckedChangeListener { _, checkedId ->
                val selectedRadioButton = root.findViewById<RadioButton>(checkedId)
                val selectedButtonName = selectedRadioButton.text.toString()

                notCompletedColorEditor.putString(
                    NOT_COMPLETED_COLOR_BUTTON_NAME,
                    selectedButtonName
                )
                notCompletedColorEditor.apply()
            }

        }
    }

}