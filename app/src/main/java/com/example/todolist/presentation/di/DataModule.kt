package com.example.todolist.presentation.di

import com.example.todolist.data.repository.RepositoryImpl
import com.example.todolist.data.storage.DatabaseStorage
import com.example.todolist.data.storage.db.Database
import com.example.todolist.domain.repository.Repository
import org.koin.dsl.module


val dataModule = module {

    single<DatabaseStorage> {
        Database(context = get())
    }

    single<Repository> {
        RepositoryImpl(databaseHelper = get())
    }
}
