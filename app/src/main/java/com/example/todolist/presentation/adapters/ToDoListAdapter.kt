package com.example.todolist.presentation.adapters

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.R
import com.example.todolist.databinding.TodoItemBinding
import com.example.todolist.domain.models.ToDoEntity
import com.example.todolist.presentation.utils.Constants

class ToDoListAdapter(
    private val context: Context,
    private val completedColorPreferences: SharedPreferences,
    private val notCompletedColorPreferences: SharedPreferences,
    private val editIconClickListener: (ToDoEntity) -> Unit,
    private val deleteIconClickListener: (ToDoEntity) -> Unit,
    private val checkboxClickListener: (ToDoEntity, Boolean) -> Unit
) : RecyclerView.Adapter<ToDoListAdapter.ViewHolder>() {

    private var listOfToDo: List<ToDoEntity> = mutableListOf()

    private val differCallback =
        object : DiffUtil.ItemCallback<ToDoEntity>() {
            override fun areItemsTheSame(
                oldItem: ToDoEntity,
                newItem: ToDoEntity
            ): Boolean {
                return oldItem.toDoId == newItem.toDoId
            }

            override fun areContentsTheSame(
                oldItem: ToDoEntity,
                newItem: ToDoEntity
            ): Boolean {
                return oldItem == newItem
            }
        }
    private val differ = AsyncListDiffer(this, differCallback)

    fun setNewContent(newContentList: List<ToDoEntity>) {
        listOfToDo = newContentList
        differ.submitList(newContentList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val binding = TodoItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val toDoItem = listOfToDo[position]
        holder.bind(toDoItem)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class ViewHolder(private val binding: TodoItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private val completedColorName =
            completedColorPreferences.getString(Constants.COMPLETED_COLOR_BUTTON_NAME, "")

        private val notCompletedColorName =
            notCompletedColorPreferences.getString(Constants.NOT_COMPLETED_COLOR_BUTTON_NAME, "")


        fun bind(toDoItem: ToDoEntity) {

            binding.apply {

                tvTodoName.text = toDoItem.toDoTitle

                ivEditIcon.setOnClickListener {
                    editIconClickListener(toDoItem)
                }

                checkbox.setOnCheckedChangeListener { _, isChecked ->
                    checkboxClickListener(toDoItem, isChecked)

                    if (isChecked) {
                        when (completedColorName) {
                            "Green" -> tvTodoName.setTextColor(
                                ContextCompat.getColor(
                                    context,
                                    R.color.green
                                )
                            )

                            "Blue" -> tvTodoName.setTextColor(
                                ContextCompat.getColor(
                                    context,
                                    R.color.blue
                                )
                            )

                            "Yellow" -> tvTodoName.setTextColor(
                                ContextCompat.getColor(
                                    context,
                                    R.color.yellow
                                )
                            )
                        }
                    } else {
                        when (notCompletedColorName) {
                            "Brown" -> tvTodoName.setTextColor(
                                ContextCompat.getColor(
                                    context,
                                    R.color.brown
                                )
                            )

                            "Pink" -> tvTodoName.setTextColor(
                                ContextCompat.getColor(
                                    context,
                                    R.color.pink
                                )
                            )

                            "Violet" -> tvTodoName.setTextColor(
                                ContextCompat.getColor(
                                    context,
                                    R.color.violet
                                )
                            )
                        }
                    }

                }

                checkbox.isChecked = toDoItem.toDoCompleted == 1

                if (checkbox.isChecked) {
                    when (completedColorName) {
                        "Green" -> tvTodoName.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.green
                            )
                        )

                        "Blue" -> tvTodoName.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.blue
                            )
                        )

                        "Yellow" -> tvTodoName.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.yellow
                            )
                        )
                    }
                } else {
                    when (notCompletedColorName) {
                        "Brown" -> tvTodoName.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.brown
                            )
                        )

                        "Pink" -> tvTodoName.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.pink
                            )
                        )

                        "Violet" -> tvTodoName.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.violet
                            )
                        )
                    }
                }

                ivDeleteIcon.setOnClickListener {
                    deleteIconClickListener(toDoItem)
                }
            }
        }
    }
}