package com.example.todolist.presentation

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todolist.domain.models.NewListEntity
import com.example.todolist.domain.usecase.DeleteNewListItemUseCase
import com.example.todolist.domain.usecase.GetAllNewListItemsUseCase
import com.example.todolist.domain.usecase.SaveNewListItemUseCase
import com.example.todolist.domain.usecase.UpdateNewListItemUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MainViewModel(
    private val saveNewListItemUseCase: SaveNewListItemUseCase,
    private val getAllNewListItemsUseCase: GetAllNewListItemsUseCase,
    private val deleteNewListItemUseCase: DeleteNewListItemUseCase,
    private val updateNewListItemUseCase: UpdateNewListItemUseCase
) : ViewModel() {

    init {
        getAllNewListItems()
    }

    private val _state = MutableStateFlow<ScreenState>(ScreenState.Loading)
    val state: StateFlow<ScreenState> = _state

    fun saveNewListItem(title: String) {
        viewModelScope.launch {
            try {
                val newListEntity = NewListEntity(listId = 0 ,newListTitle = title)
                saveNewListItemUseCase(newList = newListEntity)
            } catch (e: Exception) {
                _state.value = ScreenState.Error("Something went wrong during saving the list item")
            }
        }
    }

    fun getAllNewListItems() {
        viewModelScope.launch {
            try {
                val allNewListItems = getAllNewListItemsUseCase()

                Log.d("iii", allNewListItems.toString())

                _state.value = ScreenState.NewListSuccess(allNewListItems)
            } catch (e: Exception) {
                _state.value =
                    ScreenState.Error("Something went wrong during getting the list of all items")
            }
        }
    }

    fun deleteNewListItem(id: Long, title: String) {
        viewModelScope.launch {
            try {
                val newListEntity = NewListEntity(listId = id, newListTitle = title)
                deleteNewListItemUseCase(newList = newListEntity)
            } catch (e: Exception) {
                _state.value =
                    ScreenState.Error("Something went wrong during deleting the list item")
            }
        }
    }

    fun updateNewListItemTitle(id: Long ,oldTitle: String, updatedTitle: String) {
        viewModelScope.launch {
            try {
                val updatedListEntity = NewListEntity(listId = id, newListTitle = updatedTitle)
                updateNewListItemUseCase(oldTitle, updatedList = updatedListEntity)
            } catch (e: Exception) {
                _state.value =
                    ScreenState.Error("Something went wrong during updating the list item")
            }
        }
    }
}