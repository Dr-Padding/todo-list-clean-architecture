package com.example.todolist.presentation.models

data class NewList(
    val newListTitle: String
)