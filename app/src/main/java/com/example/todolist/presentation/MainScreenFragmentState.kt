package com.example.todolist.presentation

import com.example.todolist.domain.models.NewListEntity
import com.example.todolist.domain.models.ToDoEntity


sealed class ScreenState {
    object Loading : ScreenState()
    data class NewListSuccess(val items: List<NewListEntity>) : ScreenState()
    data class ToDoSuccess(val items: List<ToDoEntity>) : ScreenState()
    data class Error(val message: String) : ScreenState()
}
