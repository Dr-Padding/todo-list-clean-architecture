package com.example.todolist.presentation.utils

object Constants {
    const val NEW_LIST_ID = "newListId"
    const val NEW_LIST_TITLE = "newListTitle"
    const val COMPLETED_COLOR_SHARED_PREFS = "completedColorPreferences"
    const val NOT_COMPLETED_COLOR_SHARED_PREFS = "notCompletedColorPreferences"
    const val COMPLETED_COLOR_BUTTON_NAME = "completed_color_button_name"
    const val NOT_COMPLETED_COLOR_BUTTON_NAME = "not_completed_color_button_name"
}
