package com.example.todolist.presentation.di

import com.example.todolist.presentation.MainViewModel
import com.example.todolist.presentation.ToDoViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {
    viewModel {
        MainViewModel(
            saveNewListItemUseCase = get(),
            getAllNewListItemsUseCase = get(),
            deleteNewListItemUseCase = get(),
            updateNewListItemUseCase = get()
        )
    }

    viewModel {
        ToDoViewModel(
            saveToDoItemUseCase = get(),
            getAllToDoItemsUseCase = get(),
            updateToDoCheckBoxUseCase = get(),
            deleteToDoItemUseCase = get(),
            updateToDoTitleUseCase = get()
        )
    }
}