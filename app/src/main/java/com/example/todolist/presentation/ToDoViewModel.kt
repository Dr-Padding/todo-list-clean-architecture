package com.example.todolist.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todolist.domain.models.ToDoEntity
import com.example.todolist.domain.usecase.DeleteToDoItemUseCase
import com.example.todolist.domain.usecase.GetAllToDoItemsUseCase
import com.example.todolist.domain.usecase.SaveToDoItemUseCase
import com.example.todolist.domain.usecase.UpdateToDoCheckBoxUseCase
import com.example.todolist.domain.usecase.UpdateToDoTitleUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class ToDoViewModel(
    private val saveToDoItemUseCase: SaveToDoItemUseCase,
    private val getAllToDoItemsUseCase: GetAllToDoItemsUseCase,
    private val updateToDoCheckBoxUseCase: UpdateToDoCheckBoxUseCase,
    private val deleteToDoItemUseCase: DeleteToDoItemUseCase,
    private val updateToDoTitleUseCase: UpdateToDoTitleUseCase
) : ViewModel() {

    private val _state = MutableStateFlow<ScreenState>(ScreenState.Loading)
    val state: StateFlow<ScreenState> = _state


    fun saveToDoItem(title: String, toDoListId: Long) {
        viewModelScope.launch {
            try {
                val toDoEntity = ToDoEntity(
                    toDoId = 0,
                    toDoTitle = title,
                    toDoCompleted = 0,
                    toDoListId = toDoListId
                )
                saveToDoItemUseCase(toDoEntity)
            } catch (e: Exception) {
                _state.value = ScreenState.Error("Something went wrong during saving the TODO item")
            }
        }
    }

    fun getAllToDoItems(listId: Long) {
        viewModelScope.launch {
            try {
                _state.value = ScreenState.ToDoSuccess(getAllToDoItemsUseCase(listId))
            } catch (e: Exception) {
                _state.value =
                    ScreenState.Error("Something went wrong during getting the list of all TODO items")
            }
        }
    }

    fun deleteToDoItem(toDoId: Long, toDoTitle: String, completed: Int, toDoListId: Long) {
        viewModelScope.launch {
            try {
                val toDoEntity = ToDoEntity(
                    toDoId = toDoId,
                    toDoTitle = toDoTitle,
                    toDoCompleted = completed,
                    toDoListId = toDoListId
                )
                deleteToDoItemUseCase(toDoItem = toDoEntity)
            } catch (e: Exception) {
                _state.value =
                    ScreenState.Error("Something went wrong during deleting the list item")
            }
        }
    }

    fun updateToDoItemTitle(
        toDoId: Long,
        oldTitle: String,
        updatedTitle: String,
        completed: Int,
        toDoListId: Long
    ) {
        viewModelScope.launch {
            try {
                val updatedToDoEntity = ToDoEntity(
                    toDoId = toDoId,
                    toDoTitle = updatedTitle,
                    toDoCompleted = completed,
                    toDoListId = toDoListId
                )
                updateToDoTitleUseCase(oldTitle, updatedToDo = updatedToDoEntity)
            } catch (e: Exception) {
                _state.value =
                    ScreenState.Error("Something went wrong during updating the list item")
            }
        }
    }

    fun updateToDoCheckBox(toDoId: Long, title: String, completed: Int, toDoListId: Long) {
        viewModelScope.launch {
            try {
                val toDoEntity = ToDoEntity(
                    toDoId = toDoId,
                    toDoTitle = title,
                    toDoCompleted = completed,
                    toDoListId = toDoListId
                )
                updateToDoCheckBoxUseCase(toDoEntity)
            } catch (e: Exception) {
                _state.value =
                    ScreenState.Error("Something went wrong during updating toDo checkbox")
            }
        }
    }

}