package com.example.todolist.presentation.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.databinding.NewListItemBinding
import com.example.todolist.domain.models.NewListEntity

class NewListAdapter(
    private val context: Context,
//    private val listOfNewList: MutableList<NewListEntity>,
    private val titleClickListener: (NewListEntity) -> Unit,
    private val iconClickListener: (NewListEntity, Int) -> Unit
) : RecyclerView.Adapter<NewListAdapter.ViewHolder>() {


    private var listOfNewList: List<NewListEntity> = mutableListOf()

    private val differCallback =
        object : DiffUtil.ItemCallback<NewListEntity>() {
            override fun areItemsTheSame(
                oldItem: NewListEntity,
                newItem: NewListEntity
            ): Boolean {
                return oldItem.listId == newItem.listId
            }

            override fun areContentsTheSame(
                oldItem: NewListEntity,
                newItem: NewListEntity
            ): Boolean {
                return oldItem == newItem
            }
        }
    private val differ = AsyncListDiffer(this, differCallback)

    fun setNewContent(newContentList: List<NewListEntity>) {
        listOfNewList = newContentList
        differ.submitList(newContentList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val binding = NewListItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val newListItem = listOfNewList[position]
        holder.bind(newListItem)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class ViewHolder(private val binding: NewListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(newListItem: NewListEntity) {

            binding.tvNewListItem.text = newListItem.newListTitle

            binding.tvNewListItem.setOnClickListener {
                titleClickListener(newListItem)
            }

            binding.ivMoreIcon.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    iconClickListener(newListItem, position)
                }
            }
        }
    }
}