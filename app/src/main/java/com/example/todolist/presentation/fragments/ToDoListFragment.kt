package com.example.todolist.presentation.fragments

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.todolist.R
import com.example.todolist.databinding.FragmentTodoListBinding
import com.example.todolist.domain.models.ToDoEntity
import com.example.todolist.presentation.MainViewModel
import com.example.todolist.presentation.ScreenState
import com.example.todolist.presentation.ToDoViewModel
import com.example.todolist.presentation.adapters.ToDoListAdapter
import com.example.todolist.presentation.utils.Constants
import com.example.todolist.presentation.utils.Constants.COMPLETED_COLOR_BUTTON_NAME
import com.example.todolist.presentation.utils.Constants.NEW_LIST_ID
import com.example.todolist.presentation.utils.Constants.NEW_LIST_TITLE
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class ToDoListFragment : Fragment(R.layout.fragment_todo_list) {

    private val binding by viewBinding(FragmentTodoListBinding::bind)
    private val viewModel by viewModel<ToDoViewModel>()
    private lateinit var adapter: ToDoListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpAdapter()
        initViews()
        setObservers()
    }

    private fun setUpAdapter() {

        val completedColorPreferences = requireContext().getSharedPreferences(
            Constants.COMPLETED_COLOR_SHARED_PREFS,
            Context.MODE_PRIVATE
        )

        val notCompletedColorPreferences = requireContext().getSharedPreferences(
            Constants.NOT_COMPLETED_COLOR_SHARED_PREFS,
            Context.MODE_PRIVATE
        )

        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapter = ToDoListAdapter(requireContext(),
            editIconClickListener = { toDoEntity ->
                // Handle Edit option
                val oldTitle = toDoEntity.toDoTitle

                val builder = AlertDialog.Builder(requireContext())
                builder.setTitle(R.string.update_todo_item)
                val input = EditText(requireContext())
                input.setSingleLine()
                input.setText(oldTitle)
                builder.setView(input)

                builder.setPositiveButton(R.string.update) { _, _ ->
                    val updatedTitle = input.text.toString()

                    viewModel.updateToDoItemTitle(
                        toDoEntity.toDoId,
                        oldTitle,
                        updatedTitle,
                        toDoEntity.toDoCompleted,
                        toDoEntity.toDoListId
                    )
                    viewModel.getAllToDoItems(toDoEntity.toDoListId)
                }
                builder.setNegativeButton(R.string.cancel) { dialog, _ ->
                    dialog.cancel()
                }
                builder.show()
            },
            deleteIconClickListener = { toDoEntity ->
                // Handle delete click

                showDeleteConfirmationDialog(requireContext()) {

                    viewModel.deleteToDoItem(
                        toDoEntity.toDoId,
                        toDoEntity.toDoTitle,
                        toDoEntity.toDoCompleted,
                        toDoEntity.toDoListId
                    )
                    viewModel.getAllToDoItems(toDoEntity.toDoListId)
                }
            },
            checkboxClickListener = { toDoEntity, isCheckedStatus ->
                val checkedStatusInt = if (isCheckedStatus) 1 else 0
                viewModel.updateToDoCheckBox(
                    toDoEntity.toDoId,
                    toDoEntity.toDoTitle,
                    checkedStatusInt,
                    toDoEntity.toDoListId
                )
            },
            completedColorPreferences = completedColorPreferences,
            notCompletedColorPreferences = notCompletedColorPreferences
        )
        binding.recyclerView.adapter = adapter
    }

    private fun initViews() {
        val listId = arguments?.getLong(NEW_LIST_ID)
        val listTitle = arguments?.getString(NEW_LIST_TITLE)

        if (listId != null) {
            viewModel.getAllToDoItems(listId)
        }

        binding.apply {

            toolbar.title = listTitle

            fab.setOnClickListener {
                val builder = AlertDialog.Builder(requireContext())
                builder.setTitle(R.string.add_todo_item)
                val input = EditText(requireContext())
                input.setSingleLine()
                builder.setView(input)

                builder.setPositiveButton(R.string.add) { _, _ ->
                    val enteredString = input.text.toString()
                    if (listId != null) {
                        viewModel.saveToDoItem(enteredString, listId)
                    }
                    if (listId != null) {
                        viewModel.getAllToDoItems(listId)
                    }
                }
                builder.setNegativeButton(R.string.cancel) { dialog, _ ->
                    dialog.cancel()
                }
                builder.show()
            }
        }
    }

    private fun setObservers() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                viewModel.state.onEach { state ->
                    when (state) {
                        is ScreenState.Loading -> showLoading()
                        is ScreenState.ToDoSuccess -> updateRVContent(state.items)
                        is ScreenState.Error -> showError(state.message)
                        else -> {}
                    }
                }.launchIn(this)
            }
        }
    }

    private fun updateRVContent(listOfToDo: List<ToDoEntity>) {
        adapter.setNewContent(listOfToDo)
    }

    private fun showLoading() {
        // show a progress bar
    }

    private fun showError(message: String) {
        // show a toast or display an error view
    }

    private fun showDeleteConfirmationDialog(context: Context, onConfirm: () -> Unit) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Are you sure?")
            .setMessage("Do you want to delete this item?")
            .setPositiveButton("Continue") { dialog, _ ->
                // Call the onConfirm function when "Continue" is clicked
                onConfirm.invoke()
                dialog.dismiss()
            }
            .setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }

        val dialog = builder.create()
        dialog.show()
    }

}